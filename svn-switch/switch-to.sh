#!/bin/bash
### switch the svn repository to which the working copy is connected

### go to the top directory of the working copy
cd $(dirname $0)
cd ..

### include the switch-state variables ($repo_list and $current_repo)
. svn-switch/switch-state

### check that there is an argument
if [ "$1" = "" ]
then
  echo "Current repository  : $current_repo"
  echo "List of Repositories: $repo_list"
  echo "Usage: $0 target_repository"
  exit 0
fi

### get the target repository 
target_repo=$1

### backup the .svn directories of the current repository to a .tgz file
find . -name '\.svn' | xargs tar cfz svn-switch/svn_$current_repo.tgz

### clean (delete) the .svn directories of the current repository
find . -name '\.svn' | xargs rm -rf

### restore the .svn directories of the target repository
tar xfz svn-switch/svn_$target_repo.tgz

### save the state variables ($repo_list and $current_repo)
cat << EOF > svn-switch/switch-state
repo_list="$repo_list"
current_repo=$target_repo
EOF

