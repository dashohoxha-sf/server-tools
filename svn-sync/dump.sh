#!/bin/bash
### dump the latest revisions of the given project repository

### go to this directory
cd $(dirname $0)

### get the project name
if [ "$1" = "" ]
then
  echo "Usage: $0 project-name"
  exit 1
fi
proj=$1

### load the config file
. config.sh

### get the range of the revisions to be dumped
r1=$(svnlook youngest $backup_repo/$proj)
let r1++
r2=$(svnlook youngest $working_repo/$proj)

### check that there are local revisions
if [[ $r1 > $r2 ]]; then exit 0; fi

### variables
repo="$working_repo/$proj"
file="$proj-$r1-$r2.svndump"

### dump the local revisions
echo "svnadmin dump $repo -r $r1:$r2 --incremental > $file"
svnadmin dump $repo -r $r1:$r2 --incremental > $file

### zip the dump file
gzip $file

