#!/bin/bash
### get local modifications of all the repositories and backup them

### go to this directory
cd $(dirname $0)

### get config vars
. config.sh

### dump all the projects
rm -rf *.gz
projects=$(ls $working_repo)
for proj in $projects
do
  ./dump.sh $proj
done

### put them to removable storage
cd ..
tar cfz sf-svn-repo-sync.tgz svn-sync/
cp -f sf-svn-repo-sync.tgz /media/disk/diff/

rm sf-svn-repo-sync.tgz
cd svn-sync/

