#!/bin/bash
### load the given dumpfile to a repository

### go to this directory
cd $(dirname $0)

### get the file name
if [ "$1" = "" ]
then
  echo "Usage: $0 dumpfile.gz"
  exit 1
fi
file=$1

### load the config file
. config.sh

### unzip the file
gunzip $file
file=${file%\.gz}

### load the dumped revisions
proj=${file%%-[0-9]*}  ## get the project name
echo "cat $file | svnadmin load $main_repo/$proj"
cat $file | svnadmin load $main_repo/$proj

