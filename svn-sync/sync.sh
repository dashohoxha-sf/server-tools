#!/bin/bash
### load all the dump files

### go to this directory
cd $(dirname $0)

### load all files
files=$(ls *.gz)
for file in $files
do
  ./load.sh $file
done

