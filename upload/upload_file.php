<?php
//variables url and folder come from the html form
$url = $_POST['url'];
$folder = $_POST['folder'];
$folder = check_folder($folder);

/*
print "
<script language='javascript'>
  alert('$folder');
</script>
"; //debug
*/

$upload_path = "/home/groups/p/ph/phplg/htdocs/upload/files/$folder/";

$error = $_FILES['file']['error'];
switch ($error)
{
 case UPLOAD_ERR_INI_SIZE:
   $upload_msg = "The uploaded file exceeds the upload_max_filesize directive in php.ini.";
   break;

 case UPLOAD_ERR_FORM_SIZE:
   $upload_msg = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form.";
   break;

 case UPLOAD_ERR_PARTIAL:
   $upload_msg = "The uploaded file was only partially uploaded. Please try again.";
   break;

 case UPLOAD_ERR_NO_FILE:
   $upload_msg = "No file was uploaded. Please try again.";
   break;

 default:
 case UPLOAD_ERR_OK:
   $tmp_name = $_FILES['file']['tmp_name']; //uploaded file
   $file_name = $_FILES['file']['name'];    //local filename
   $fname = basename($file_name);
   $dest_file = $upload_path.$fname;  //where uploaded file will be saved
   if (move_file($tmp_name, $dest_file))
     {
       $upload_msg = "File uploaded successfully.";
     }
   else
     {
       $upload_msg = "File not uploaded, there is some error.";
     }
   break;
}

include_once 'upload_message.html';

function move_file($tmp_file, $dest_file)
{
  if (!is_uploaded_file($tmp_file))  return false;

  $dest_dir = dirname($dest_file);
  exec("mkdir -p $dest_dir");
  exec("mv -f $tmp_file $dest_file");

  return file_exists($dest_file);
}

function check_folder($folder)
{
  $folder = trim($folder);
  $folder = str_replace('../', '', $folder);
  $folder = str_replace('./', '', $folder);
  $folder = str_replace('//', '/', $folder);
  $folder = ereg_replace('^/', '', $folder);

  return $folder;
}
?>
