//-*-C-*-

function upload_file()
{
  var new_win = window.open('', 'upload_message', 'width=200,height=150');
  var file = document.upload.file.value;
  var win_content = "<html>\n"
    + "<head>\n"
    + " <title>Uploading: " + file + " </title>\n"
    + " <style>\n"
    + "   body \n"
    + "   { \n"
    + "     background-color: #f8fff8; \n"
    + "     margin: 10; \n"
    + "     font-family: sans-serif; \n"
    + "     font-size: 10pt; \n"
    + "     color: #000066; \n"
    + "   } \n"
    + "   h1 { font-size: 12pt; color: #000066; } \n"
    + "   h2 { font-size: 10pt; color: #aa0000; } \n"
    + " </style>\n"
    + "</head>\n"
    + "<body>\n"
    + "  <h1>Uploading: " + file + " </h1>\n"
    + "  <h2>Please wait...<h2>\n"
    + "</body>\n"
    + "</html>\n";
  new_win.document.write(win_content);

  document.upload.url.value = document.URL;
  document.upload.submit();

  //uncomment this line, to open another page after making the upload
  //location.href = 'next_page.html';
}
