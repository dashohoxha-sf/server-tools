#!/bin/bash
### Copy in the directory files all the files listed in 'files.txt'.
### Copy them recursivly (in case that they are directories), only
### if the timestamp has changed (the original file has been modified),
### and preserving the attributes (timestamp etc.)
### Copy also the file 'files.txt' itself, and generate a list of
### installed packages as well.

### go to the directory of this script
cd $(dirname $0)

### create a folder
dir=files
mkdir -p $dir

### copy files in it
file_list=$(cat files.txt)
for file in $file_list
do
  file1=${file:1}  ## remove the leading slash
  path=$(dirname $file1)
  mkdir -p $dir/$path/
  cp -rup $file $dir/$path/
done

### copy also files.txt
cp -Lup files.txt $dir/

### get a list of packages
rpm -qa | sort > $dir/pkglist.txt

