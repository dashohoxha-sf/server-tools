#!/bin/bash
### Copy files and commit them to the svn repository.
### If there are any changes, they will be sent by
### email to the admin(s).
###
### This script can be called periodically by a cron job.

### go to the directory of this script
cd $(dirname $0)

### copy any file that is modified
./copy-files.sh

### commit in svn
svn commit files/ --message ""

