#!/bin/bash
### initialize a subversion repository
### with the files that are to be checked

### go to the directory of this script
cd $(dirname $0)

### copy files
./copy-files.sh

### create a subversion repository
svnadmin create svn-repository

### import the files in the repository
svn import files/ file://$(pwd)/svn-repository/files --message 'init'

### checkout from the repository
rm -rf files
svn checkout file://$(pwd)/svn-repository/files

### setup the post-commit script
admin_email="root"
cp -f post-commit.tmpl post-commit
echo "$(pwd)/commit-email.pl \"\$REPOS\" \"\$REV\" $admin_email" >> post-commit
chmod +x post-commit
ln -s ../../post-commit svn-repository/hooks/post-commit

